package threads;

import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

public class LetterKiller implements Runnable{

    ArrayList<Thread> threads;

    public static final float  DEFAULT_PROBABILITY_TO_HIT = 0.75F;
    public static final int DEFAULT_NUMBER_OF_TRIES = 30;
    public static final int DEFAULT_MS_BETWEEN_TRIES = 100;

    private int currentElement = 0;
    private float probabilityToHit = DEFAULT_PROBABILITY_TO_HIT;
    private int numberOfTries = DEFAULT_NUMBER_OF_TRIES;
    private int msBetweenTries = DEFAULT_MS_BETWEEN_TRIES;


    public LetterKiller(ArrayList<Thread> threads){

        this.threads = threads;

    }


    @Override
    public void run() {

        System.out.println( Thread.currentThread().getName() + " STARTED!");

        for ( int currentTry = 0; currentTry< numberOfTries; currentTry++){

            try {
                Thread.sleep(msBetweenTries);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if(ThreadLocalRandom.current().nextDouble(1.0) <= probabilityToHit){

                threads.get(currentElement++).interrupt();

                if (currentElement == threads.size()){

                    break;
                }

            }


        }

        System.out.println( Thread.currentThread().getName() + " STOPPED!");


    }
}
