package threads;

import java.util.ArrayList;

public class Ejercicio3 {



    public static void main(String[] args) {

        System.out.println("---- EJERCICIO 2 - Empezando la simulación ----");
        System.out.println();

        Contador c = new Contador();

        ArrayList<LetterWriter> letters = new ArrayList<>();
        ArrayList<Thread> threadsLetters = new ArrayList<>();

        for ( int i = 97; i<=122; i++){
            LetterWriter lw = new LetterWriter((char)i,c);
            letters.add(lw);
            threadsLetters.add(new Thread(lw,lw.toString()));
        }

        Thread lkTh = new Thread(new LetterKiller(threadsLetters),"LetterKiller");

        for (Thread t : threadsLetters){
            t.start();
        }

        lkTh.start();

        try {
            for (Thread t : threadsLetters){
                t.join();
            }

            lkTh.join();

        } catch (InterruptedException e) {
            System.err.println(e.getLocalizedMessage());
            return;
        }



        System.out.println();
        System.out.println("---- EJERCICIO 3 - Finalizando la simulación ----");
        System.out.println();
        System.out.println("--------------------- TOTALES -------------------");
        System.out.println(c.toString());
        System.out.println("-------------------------------------------------");
    }

}
