package es.cipfpbatoi.psp.extraordinaria.threads;

public class Ejercicio1 {

    public static void main(String[] args) {

        int numberOfIterations = 10;
        int numberOfIterationsBeforeWait = 9;
        int waitMsBetweenIterations = 100;

        LetterWriter a = new LetterWriter('a',numberOfIterationsBeforeWait,numberOfIterations,waitMsBetweenIterations);
        LetterWriter e = new LetterWriter('e',numberOfIterationsBeforeWait,numberOfIterations,waitMsBetweenIterations);
        LetterWriter i = new LetterWriter('i',numberOfIterationsBeforeWait,numberOfIterations,waitMsBetweenIterations);
        LetterWriter o = new LetterWriter('o',numberOfIterationsBeforeWait,numberOfIterations,waitMsBetweenIterations);
        LetterWriter u = new LetterWriter('u',numberOfIterationsBeforeWait,numberOfIterations,waitMsBetweenIterations);

        Thread[] writer = new Thread[5];

        Thread aTh = writer[0] = new Thread(a,a.toString());
        Thread eTh = writer[1] = new Thread(e,e.toString());
        Thread iTh = writer[2] = new Thread(i,i.toString());
        Thread oTh = writer[3] = new Thread(o,o.toString());
        Thread uTh = writer[4] = new Thread(u,u.toString());

        System.out.println("---- EJERCICIO 1 - Empezando la simulación ----");
        System.out.println();


        e.setThreadToWait(aTh);
        i.setThreadToWait(eTh);
        o.setThreadToWait(iTh);
        u.setThreadToWait(oTh);

        for (Thread t:writer) {

            t.start();

        }

        for (Thread t:writer){
            try {
                t.join();
            } catch (InterruptedException ex) {
                System.err.println("The thread was interrupted");
                System.exit(1);
            }
        }

        System.out.println();
        System.out.println();
        System.out.println("---- EJERCICIO 1 - Finalizando la simulación ----");

    }

}
