package es.cipfpbatoi.psp.extraordinaria.threads;

public class LetterWriter implements Runnable{

    private final char character;
    private Thread threadToWait;
    private int numberOfIterationsBeforeWait = -1;
    private final int numberOfIterations;
    public final int waitMsBetweenIterations;
    public static final int DONT_WAIT = -1;
    public static final int DEFAULT_WAIT_MS = 100;
    public static final int DEFAULT_NUMBER_OF_ITERATIONS = 10;

    private Contador contador;

    /**
     * Creates a new object of the class LetterWriter and doesn't launch the wait
     * @param character Character to Write also sets the thread name to this value
     */
    public LetterWriter(char character){
        this(character,null);
    }

    public LetterWriter(char character, Contador contador){
        this(character,
                DONT_WAIT,
                DEFAULT_NUMBER_OF_ITERATIONS,
                DEFAULT_WAIT_MS,
                contador);
    }

    /**
     * Creates a new object of the class LetterWriter and after the operations
     * in numberOfIterationsBeforeWait are done, will wait to the threads in
     *
     * @param character Character to Write also sets the thread name to this value
     * @param numberOfIterationsBeforeWait Iterations completed before it waits to the finish
     * @param numberOfIterations Total iterations
     * @param waitMsBetweenIterations ms to wait between iterations
     *
     */
    public LetterWriter(char character, int numberOfIterationsBeforeWait, int numberOfIterations, int waitMsBetweenIterations, Contador contador)
    {
        this.character = character;
        this.numberOfIterationsBeforeWait = numberOfIterationsBeforeWait;
        this.numberOfIterations = numberOfIterations;
        this.waitMsBetweenIterations = waitMsBetweenIterations;
        this.contador = contador;
    }

    @Override
    public void run() {

        for (int i = 0; i<numberOfIterations; i++){

            System.out.print(character);

            if (null != contador) {
                contador.incrementCounterForCharacter(character);
            }

            try {
                Thread.sleep(waitMsBetweenIterations);



            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

        }

    }

    @Override
    public String toString() {
        return String.valueOf(getCharacter());
    }

    public Thread getThreadToWait() {
        return threadToWait;
    }

    public void setThreadToWait(Thread threadToWait) {
        this.threadToWait = threadToWait;
    }

    public char getCharacter() {
        return character;
    }
}
