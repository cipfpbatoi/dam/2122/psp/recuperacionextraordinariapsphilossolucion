package es.cipfpbatoi.psp.extraordinaria.threads;

public class Ejercicio2 {

    public static void main(String[] args) {

        Contador contador = new Contador();

        LetterWriter a = new LetterWriter('a',contador);
        LetterWriter e = new LetterWriter('e',contador);
        LetterWriter i = new LetterWriter('i',contador);
        LetterWriter o = new LetterWriter('o',contador);
        LetterWriter u = new LetterWriter('u',contador);

        Thread[] writer = new Thread[5];

        writer[0] = new Thread(a,a.toString());
        writer[1] = new Thread(e,e.toString());
        writer[2] = new Thread(i,i.toString());
        writer[3] = new Thread(o,o.toString());
        writer[4] = new Thread(u,u.toString());

        System.out.println("---- EJERCICIO 2 - Empezando la simulación ----");
        System.out.println();

        for (Thread t:writer) {

            t.start();

        }

        for (Thread t:writer){
            try {
                t.join();
            } catch (InterruptedException ex) {
                System.err.println("The thread was interrupted");
                System.exit(1);
            }
        }

        System.out.println();
        System.out.println("---- EJERCICIO 2 - Finalizando la simulación ----");
        System.out.println();
        System.out.println("--------------------- TOTALES -------------------");
        System.out.println(contador.toString());
        System.out.println("-------------------------------------------------");

    }

}
