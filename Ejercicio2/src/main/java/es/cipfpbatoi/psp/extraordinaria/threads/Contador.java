package es.cipfpbatoi.psp.extraordinaria.threads;

import java.util.HashMap;
import java.util.Map;

public class Contador {

    private Map<Character,Integer> totales = new HashMap<Character,Integer>();

    public Contador(){

    }

    public synchronized void incrementCounterForCharacter(char c)
    {
        if (!totales.containsKey(c)){
            totales.put(c,1);
        } else {
            totales.replace(c,totales.get(c)+1);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        int total = 0;

        for (char c : totales.keySet()){

            sb.append("[").append(c).append("]").append("=").append(totales.get(c)).append(System.lineSeparator());
            total+=totales.get(c);

        }
        sb.append(System.lineSeparator());
        sb.append("[GLOBAL COUNT]=").append(total).append(System.lineSeparator());

        return sb.toString();
    }
}
